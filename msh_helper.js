(function(w, d) {

	var styleProps =
		"html, body {margin: 0; padding: 0; font: 13px/1.2 Arial, Helvetica, sans-serif} " +
		"body {overflow-y: scroll}" +
		"ul {list-style: none; margin: 0; padding: 0}" +
		".it {margin: 0; padding: 0 8px; height: 32px; line-height: 17px; background-color: #fff; border: 1px solid #ccc; border-radius: 3px; box-shadow: inset 0 0 2px 1px rgba(0,0,0,.07); font-size: 13px; color: #333; box-sizing: border-box; vertical-align: top;}" +
		".it + .it {margin-left: 1%}" +
		".it::-webkit-search-decoration {display: none}" +
		".msg_helper_id .it {width: 48%}" +
		".msg_helper_search .it { width: 80% }" +
		".msg_helper_search-btn {box-sizing: border-box; width: 16%; margin: 0 0 0 1%; height: 32px; cursor: pointer; white-space: nowrap; border-radius: 3px; border: 0; font: 500 14px/1.2 arial,helvetica,sans-serif; color: #fff; text-align: center; background: #1daa09 linear-gradient(to bottom,#78d81f,#1daa09); box-shadow: inset 0 -1px 0 0 #21900d; text-shadow: 0 1px 0 #458f11; vertical-align: top;}" +
		".msg_helper {padding: 10px}" +
		".msg_helper_h {border-bottom: 1px #666 dotted; margin-bottom: 10px; }" +
		".msg_helper_id, .msg_helper_search {margin-bottom: 10px}" +
		".msg_helper_filter {position: relative;}" +
		".msg_helper_filter-cball {vertical-align: top; margin-top: 10px; position: absolute;}" +
		".msg_helper_filter-it {width: 90%; margin-left: 7%}" +
		".msg_helper_filter-cball, .msg_helper_filter-cb {position: absolute; top: 0; left: 0;}" +
		".msg_helper_filter-tx {display: inline-block; margin-left: 7%}" +
		".msg_helper_filter-i { position: relative; padding: 2px 0 }" +
		".msg_helper_filter-lst { padding: 5px 0 0 }" +
		".msg_helper_close {position: fixed; top: 0; right: 0; padding: 2px; cursor: pointer}";

	var sections = [
		{
			title: "Личное сообщение",
			id: 7
		},
		{
			title: "Сообщения на форуме",
			id: 8
		},
		{
			title: "Сообщения на двухуровневом форуме",
			id: 25
		},
		{
			title: "Личный фотоальбом: название, описание",
			id: 11
		},
		{
			title: "Личный фотоальбом: comment",
			id: 111
		},
		{
			title: "Подпись к фотографии",
			id: 9
		},
		{
			title: "Комментарий к фотографии",
			id: 10
		},
		{
			title: "Фотометки",
			id: 24
		},
		{
			title: "Загруженное видео",
			id: 47
		},
		{
			title: "Личные данные: имя, фамилия, город",
			id: 1
		},
			{
			title: "Блок о службе",
			id: 2
		},
		{
			title: "Блок о жизни",
			id: 3
		},
		{
			title: "Блок разное",
			id: 4
		},
		{
			title: "Блок об учебе",
			id: 5
		},
		{
			title: "Блок о работе",
			id: 6
		},
		{
			title: "Интересы пользователя",
			id: 44
		},
		{
			title: "URL ссылки",
			id: 30
		},
		{
			title: "Alt URL ссылки",
			id: 34
		},
		{
			title: "URL картинки для ссылки",
			id: 35
		},
		{
			title: "Текст ссылки",
			id: 31
		},
		{
			title: "Комент к шаре",
			id: 26
		},
		{
			title: "Переход на внешнюю ссылку",
			id: 33
		},
		{
			title: "Склеенные личные сообщения",
			id: 41
		},
		{
			title: "Склеиные дискуссии",
			id: 42
		},
		{
			title: "Сообщения в чате",
			id: 51
		},
		{
			title: "Тема в чате",
			id: 52
		},
		{
			title: "Новость города",
			id: 54
		},
		{
			title: "Приглашение в группу",
			id: 15
		},
		{
			title: "Запрос на вступление",
			id: 16
		},
		{
			title: "Группа: название, описание",
			id: 13
		},
		{
			title: "Группа: адрес, телефон, веб-сайт",
			id: 46
		},
		{
			title: "Новости группы",
			id: 14
		},
		{
			title: "Название тем в форуме группы",
			id: 17
		},
		{
			title: "URL ссылки в теме форума группы",
			id: 117
		},
		{
			title: "Сообщения в форуме группы",
			id: 18
		},
		{
			title: "Фотоальбом в группе",
			id: 19
		},
		{
			title: "Подпись к фотографии группы",
			id: 20
		},
		{
			title: "Комментарий к фотографии группы",
			id: 21
		},
		{
			title: "Сообщение на форуме сообщества",
			id: 12
		},
		{
			title: "Пользовательский статус",
			id: 22
		},
		{
			title: "Комментарий к пользовательскому статусу",
			id: 23
		},
		{
			title: "Фото пользователя",
			id: 27
		},
		{
			title: "Фото в альбоме пользователя",
			id: 28
		},
		{
			title: "Фото в группе",
			id: 29
		},
		{
			title: "Приглашение в игру",
			id: 32
		},
		{
			title: "Комментарии к видео",
			id: 36
		},
		{
			title: "Мероприятия: название",
			id: 37
		},
		{
			title: "Мероприятия: город, место",
			id: 38
		},
		{
			title: "Мероприятия: темы",
			id: 39
		},
		{
			title: "Мероприятия: комментарии к темам",
			id: 40
		},
		{
			title: "messageType_HAPPENING_FORUM_LINK",
			id: 57
		},
		{
			title: "Мероприятия: хэш-код афиши",
			id: 45
		},
		{
			title: "Праздники",
			id: 43
		},
		{
			title: "Места",
			id: 60
		}
	]

	var elements = {};

	var template = new DDiv(
		new DClassAttr("msg_helper"),
		new DStyle(
			styleProps
		),
		new DDiv(
			new DClassAttr("msg_helper_close"),
			new DIdAttr("close"),
			"❌"
		),
		new DDiv(
			new DClassAttr("msg_helper_h"),
			new DDiv(
				new DClassAttr("msg_helper_id"),
				new DInput(
					new DIdAttr("writer"),
					new DClassAttr("msg_helper_id-it it"),
					new DAttr("type", "search"),
					new DAttr("autofocus", "true"),
					new DAttr("placeholder", "id написавшего")
				),
				new DInput(
					new DIdAttr("owner"),
					new DClassAttr("msg_helper_id-it it"),
					new DAttr("type", "search"),
					new DAttr("placeholder", "id владельца")
				)
			),
			new DDiv(
				new DClassAttr("msg_helper_search"),
				new DInput(
					new DIdAttr("search_query"),
					new DClassAttr("msg_helper_search-it it"),
					new DAttr("type", "search"),
					new DAttr("placeholder", "поисковый запрос")
				),
				new DButton(
					new DIdAttr("search_btn"),
					new DClassAttr("msg_helper_search-btn"),
					"→"
				)
			)
		),
		new DDiv(
			new DClassAttr("msg_helper_cnt"),
			new DDiv(
				new DClassAttr("msg_helper_filter"),
				new DInput(
					new DIdAttr("filter_cball"),
					new DClassAttr("msg_helper_filter-cball"),
					new DAttr("type", "checkbox")
				),
				new DInput(
					new DIdAttr("filter_it"),
					new DClassAttr("msg_helper_filter-it it"),
					new DAttr("type", "search"),
					new DAttr("placeholder", "фильтр по типам")
				),
				new DUl(
					new DIdAttr("filter_list"),
					new DClassAttr("msg_helper_filter-lst")
				)
			)
		)
	).getElementsWithId(elements);

	/* Pseudo RDK lib */

	function DTag(tagName, args) {
		var that = this;

		that.element = d.createElement(tagName);
		that.tagName = tagName;
		that.content = "";
		that.attr = {};
		that.idMap = {};

		function extend(extendableObject, sourceObject) {
			for (var id in sourceObject) {
				extendableObject[id] = sourceObject[id];
			}

			return extendableObject;
		}

		args.forEach(function(argument) {
			if (argument instanceof DTag) {
				var childNode = argument.element;
				that.element.appendChild(childNode);

				that.idMap = extend(that.idMap, argument.idMap);

				if (argument.attr.id) {
					that.idMap[argument.attr.id] = {
						id: argument.attr.id,
						node: argument.element
					}
				}
			}

			if (argument instanceof DAttr) {
				that.element.setAttribute(argument.attrName, argument.attrValue);
				that.attr[argument.attrName] = argument.attrValue;
			}

			if (typeof argument === "string") {
				that.element.textContent = argument;
				that.content = argument;
			}
		});

		that.render = function() {
			return that.element.outerHTML;
		}

		that.getElementsWithId = function (map) {
			map = extend(map, that.idMap);
			return that;
		}
	}

	function DStyle() {
		this.__proto__ = new DTag("style", Array.prototype.slice.call(arguments));
	}

	function DDiv() {
		this.__proto__ = new DTag("div", Array.prototype.slice.call(arguments));
	}

	function DSpan() {
		this.__proto__ = new DTag("span", Array.prototype.slice.call(arguments));
	}

	function DLabel() {
		this.__proto__ = new DTag("label", Array.prototype.slice.call(arguments));
	}

	function DUl() {
		this.__proto__ = new DTag("ul", Array.prototype.slice.call(arguments));
	}

	function DLi() {
		this.__proto__ = new DTag("li", Array.prototype.slice.call(arguments));
	}

	function DInput() {
		this.__proto__ = new DTag("input", Array.prototype.slice.call(arguments));
	}

	function DButton() {
		this.__proto__ = new DTag("button", Array.prototype.slice.call(arguments));
	}



	function DAttr(attrName, attrValue) {
		this.attrName = attrName;
		this.attrValue = attrValue;
	}

	function DClassAttr(className) {
		this.__proto__ = new DAttr("class", className);
	}

	function DIdAttr(idName) {
		this.__proto__ = new DAttr("id", idName);
	}

	/* /Pseudo RDK lib */

	function bindEvents(doc) {
		doc.addEventListener("click", function (e) {
			var target = e.target;

			if (target.getAttribute("id") === elements["search_btn"].id ) {
				search();
			}

			if (target.getAttribute("id") === elements["close"].id) {
				destroy();
			}
		}, false );

		doc.addEventListener("change", function (e) {
			var target = e.target;

			if (target.getAttribute("id") === elements["filter_cball"].id ) {
				for (var i = 0; i < elements["filter_cb"].node.length; i++) {
					elements["filter_cb"].node[i].checked = target.checked;
				}
			}
		}, false );

		doc.addEventListener("keydown", function (e) {
			if (e.which === 13) {
				search();
			}

			if (e.which === 27) {
				destroy();
			}
		}, false );

		doc.addEventListener("input", function (e) {
			var target = e.target;
			var filterText = "";

			if (target.getAttribute("id") === elements["filter_it"].id ) {
				filterText = elements["filter_it"].node.value;
				filterList(filterText);
			}
		}, false );
	}

	function listItemTemplate(title, id) {

		return new DLi(
			new DClassAttr("msg_helper_filter-i"),
			new DAttr("data-text", title + " " + id),
			new DLabel(
				new DClassAttr("msg_helper_filter-label"),
				new DInput(
					new DClassAttr("msg_helper_filter-cb"),
					new DAttr("type", "checkbox"),
					new DAttr("value", id)
				),
				new DSpan(
					new DClassAttr("msg_helper_filter-tx"),
					title + " (" + id + ")"
				)
			)
		).render();
	}

	function generateList() {
		var list = "";

		for (var i = 0; i < sections.length; i++) {
			list += listItemTemplate(sections[i].title, sections[i].id);
		}

		elements["filter_list"].node.innerHTML = list;

		elements["filter_nodes"] = {
			node: elements["filter_list"].node.querySelectorAll(".msg_helper_filter-i")
		};

		elements["filter_cb"] = {
			node: elements["filter_list"].node.querySelectorAll(".msg_helper_filter-cb")
		};
	}

	function filterList(filter) {
		var filter = filter || "";
		var queryArr = filter.trim().split(" ");

		for (var i = 0; i < elements["filter_nodes"].node.length; i++) {
			var value = elements["filter_nodes"].node[i].getAttribute("data-text");
			elements["filter_nodes"].node[i].style.display = "none";

			for (var j = 0; j < queryArr.length; j++ ) {
				var query = new RegExp(queryArr[j].toLowerCase(), "gi");

				if (query.test(value)) {
					elements["filter_nodes"].node[i].style.display = "block";
					break;
				}
			}
		}
	}

	function search() {
		var url = "http://spamgt.ok.ru/dk?st.cmd=adminSearchMessage&st.searchToday=off";

		function getFilterString() {
			var result = [];

			for (var i = 0; i < elements["filter_cb"].node.length; i++) {
				if (elements["filter_cb"].node[i].checked) {
					result.push(elements["filter_cb"].node[i].getAttribute("value"));
				}
			}

			return result.join(";");
		}

		url += "&st.ownerId=" + elements["writer"].node.value;
		url += "&st.discOwnerId=" + elements["owner"].node.value;
		url += "&st.searchString=" + elements["search_query"].node.value;
		url += "&st.filter=" + getFilterString();

		var win = w.open(url, "_blank");
  		win.focus();
	}

	function destroy() {
		if (w.msgHelper) {
			if (w.msgHelper.ohWasAdded) {
				d.documentElement.classList.remove("oh");
			}

			d.body.removeChild(w.msgHelper);
			w.msgHelper = null;
		}
	}

	function render() {
		if (!w.msgHelper) {

			var iframeWrapper = d.createElement("iframe");

			iframeWrapper.setAttribute("id", "msg_helper");
			iframeWrapper.setAttribute("frameborder", "0");
			iframeWrapper.setAttribute("style", "position: fixed; top: 0; right: 0; height: 100%; width: 350px; border-left: 1px #666 solid; z-index: 100000; background: #fff;");

			d.body.appendChild(iframeWrapper);

			var inter = setInterval(function() {
				// put inside function
				var doc = d.getElementById("msg_helper").contentWindow.document;

				if(doc.readyState == "complete") {
					clearInterval(inter);

					doc.body.appendChild(template.element);

					generateList();
					filterList();
					bindEvents(doc);
				}
			}, 100);

			w.msgHelper = iframeWrapper;

			if (!d.documentElement.classList.contains('oh')) {
				w.msgHelper.ohWasAdded = true;
			}

			d.documentElement.classList.add('oh');
		} else {
			destroy();
		}
	}

	render();

})(window, document);